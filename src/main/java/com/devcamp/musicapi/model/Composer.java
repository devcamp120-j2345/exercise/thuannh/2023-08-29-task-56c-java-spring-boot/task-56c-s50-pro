package com.devcamp.musicapi.model;

public class Composer extends Person{
    
    private String stagename ;

    public Composer(String firstname, String lastname, String stagename) {
        super(firstname, lastname);
        this.stagename = stagename;
    }

    public String getStagename() {
        return stagename;
    }

    public void setStagename(String stagename) {
        this.stagename = stagename;
    }

    @Override
    public String toString() {
        return "Composer [stagename=" + stagename + "]";
    }

}
