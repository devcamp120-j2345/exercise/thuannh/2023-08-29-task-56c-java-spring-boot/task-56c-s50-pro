package com.devcamp.musicapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.musicapi.model.Artist;
import com.devcamp.musicapi.model.Band;
import com.devcamp.musicapi.model.BandMember;
import com.devcamp.musicapi.model.Composer;

@Service
public class ComposerService {
           // khởi tạo đối tượng  Artist
        Artist Artist1 = new Artist("Black", "Pink", "1");
        Artist Artist2 = new Artist("Black", "Pink", "2");
        Artist Artist3 = new Artist("Black", "Pink", "3");


        // khởi tạo đối tượng bandMember 
        BandMember bMem1 = new BandMember("band 1", "member 1", "stage name 1", "kpop");
        BandMember bMem2 = new BandMember("band 2", "member 2", "stage name 2", "rock");
        BandMember bMem3 = new BandMember("band 3", "member 3", "stage name 3", "ballad");
        BandMember bMem4 = new BandMember("band 4", "member 4", "stage name 4", "hip hop");
        BandMember bMem5 = new BandMember("band 5", "member 5", "stage name 5", "chill");
        BandMember bMem6 = new BandMember("band 6", "member 6", "stage name 6", "disco");
        
    // tạo 1 arraylist artist gồm 3 artist
    public ArrayList<Artist> getArtists(){

        Artist1.setAlbums(AlbumService.getAlbum1());
        Artist2.setAlbums(AlbumService.getAlbum2());
        Artist3.setAlbums(AlbumService.getAlbum3());

        ArrayList<Artist> artists = new ArrayList<>();

        artists.add(Artist1);
        artists.add(Artist2);
        artists.add(Artist3);

        return artists ;
    }
    // tạo 1 arraylist artist gồm 3 artist 6 band members 
    public ArrayList<Composer> getComposeres(){
        
        Artist1.setAlbums(AlbumService.getAlbum1());
        Artist2.setAlbums(AlbumService.getAlbum2());
        Artist3.setAlbums(AlbumService.getAlbum3());

        ArrayList<Composer> composeres = new ArrayList<>();

        composeres.add(Artist1);
        composeres.add(Artist2);
        composeres.add(Artist3);

        composeres.add(bMem1);
        composeres.add(bMem2);
        composeres.add(bMem3);
        composeres.add(bMem4);
        composeres.add(bMem5);
        composeres.add(bMem6);

        return composeres ;
    }  

    // tạo 3 band nhạc add 6 bandmemmber ở trên zô mỗi band là 1 arraylist
    public ArrayList<Band> getBands(){

        ArrayList<BandMember> bMember1 = new ArrayList<>();

        bMember1.add(bMem1);
        bMember1.add(bMem2);
       

        ArrayList<BandMember> bMember2 = new ArrayList<>();

        bMember2.add(bMem4);
        bMember2.add(bMem5);
      

        ArrayList<BandMember> bMember3 = new ArrayList<>();

        bMember3.add(bMem5);
        bMember3.add(bMem6);

        // khởi tạo đối tượng Band và nhét dữ liệu là arraylist bmember vừa tạo ở trên zô
        Band band1 = new Band("YG",bMember1, AlbumService.getAlbum1());
        Band band2 = new Band("YG2",bMember2, AlbumService.getAlbum2());
        Band band3 = new Band("YG3",bMember3, AlbumService.getAlbum3());
    

        // tạo 1 arraylist tổng add 3 band vào
        ArrayList<Band> bands = new ArrayList<>();

        bands.add(band1);
        bands.add(band2);
        bands.add(band3);

        return bands ;
    }

}
