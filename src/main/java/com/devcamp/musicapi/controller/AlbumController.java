package com.devcamp.musicapi.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.musicapi.model.Album;
import com.devcamp.musicapi.service.AlbumService;

@RestController
public class AlbumController {
            @GetMapping("/albums")
        public ArrayList<Album> getAlllbums(){

            ArrayList<Album> albums = AlbumService.getAlbumList();

            return albums ;
        }
}
