package com.devcamp.musicapi.service;


import java.util.ArrayList;
import java.util.List;
import com.devcamp.musicapi.model.Album;
public class AlbumService {

    public static ArrayList<Album> getAlbumList(){

        // tạo 1 liststring add vào String tự do , để khi khởi tạo Albulm thì add vào 
        List<String> list1 = new ArrayList<String>();
        List<String> list2 = new ArrayList<String>();
        List<String> list3 = new ArrayList<String>();
        List<String> list4 = new ArrayList<String>();
        List<String> list5 = new ArrayList<String>();
        List<String> list6 = new ArrayList<String>();
        List<String> list7 = new ArrayList<String>();
        List<String> list8 = new ArrayList<String>();
        List<String> list9 = new ArrayList<String>();
        List<String> list10 = new ArrayList<String>();

        // add String vào list vừa khởi tạo 
        list1.add("track 1 , list 1 ");
        list2.add("track 2 , list 2");
        list3.add("track 3 , list 3");
        list4.add("track 4 , list 4");
        list5.add("track 5 , list 5");
        list6.add("track 6 , list 6");
        list7.add("track 7 , list 7");
        list8.add("track 8 , list 8");
        list9.add("track 9 , list 9");
        list10.add("track 10 , list 10");
        
        // khởi tạo đối tượng album và nhét list vừa tạo vào
        Album a1 = new Album("vol 1", list1);
        Album a2 = new Album("vol 2", list2);
        Album a3 = new Album("vol 3", list3);
        Album a4 = new Album("vol 4", list4);
        Album a5 = new Album("vol 5", list5);
        Album a6 = new Album("vol 6", list6);
        Album a7 = new Album("vol 7", list7);
        Album a8 = new Album("vol 8", list8);
        Album a9 = new Album("vol 9", list9);
        Album a10 = new Album("vol 10", list10);
        
        // khỏi tạo đối tượng allAlbum , add hết Class Album vừa khởi tạo vào
        ArrayList<Album> albums = new ArrayList<>();

        albums.add(a1);
        albums.add(a2);
        albums.add(a3);
        albums.add(a4);
        albums.add(a5);
        albums.add(a6);
        albums.add(a7);
        albums.add(a8);
        albums.add(a9);
        albums.add(a10);

        return albums ;
    }

    public static ArrayList<Album> getAlbum1(){

        // tạo list String 
        List<String> list1 = new ArrayList<String>();
        List<String> list2 = new ArrayList<String>();
        List<String> list3 = new ArrayList<String>();

        // add dữ liệu vào list
        list1.add("mmm");
        list2.add("hhh");
        list3.add("ggg");

        // khởi tạo đối tượng album
        Album vol1 = new Album("vol 1", list1);
        Album vol2 = new Album("vol 2", list2);
        Album vol3 = new Album("vol 3", list3);

        // khỏi tạo arraylist
        ArrayList<Album> albums = new ArrayList<>();

        // add đối tượng albyum vùa tạo vào arraylist 
        albums.add(vol1);
        albums.add(vol2);
        albums.add(vol3);

        return albums ;
    }
    // giống getAlbum phía trên
    public static ArrayList<Album> getAlbum2(){

        List<String> List4 = new ArrayList<String>();
        List<String> List5 = new ArrayList<String>();
        List<String> List6 = new ArrayList<String>();

        List4.add("love u hate u");
        List5.add("forever young");
        List6.add("lalalalala");

        Album vol4 = new Album("vol 4", List4);
        Album vol5 = new Album("vol 5", List5);
        Album vol6 = new Album("vol 6", List6);

        ArrayList<Album> albums = new ArrayList<>();

        albums.add(vol4);
        albums.add(vol5);
        albums.add(vol6);

        return albums ;
    }
    // giống getAlbum phía trên
    public static ArrayList<Album> getAlbum3(){

        List<String> List7 = new ArrayList<String>();
        List<String> List8 = new ArrayList<String>();
        List<String> List9 = new ArrayList<String>();
        List<String> List10 = new ArrayList<String>();

        List7.add("money");
        List8.add("shut down");
        List9.add("pink venom");
        List10.add("tally");

        Album vol7 = new Album("vol 9", List7);
        Album vol8 = new Album("vol 8", List8);
        Album vol9 = new Album("vol 9", List9);
        Album vol10 = new Album("vol 10", List10);

        ArrayList<Album> albums = new ArrayList<>();

        albums.add(vol7);
        albums.add(vol8);
        albums.add(vol9);
        albums.add(vol10);

        return albums ;
    }
  

}
