package com.devcamp.musicapi.model;

import java.util.List;

public class Artist extends Composer {
    
    private List<Album> albums;

    public Artist(String firstname, String lastname, String stagename, List<Album> album) {
        super(firstname, lastname, stagename);
        albums = album;
    }

    public Artist(String firstname, String lastname, String stagename) {
        super(firstname, lastname, stagename);
    }

    public List<Album> getAlbums() {
        return albums;
    }

    public void setAlbums(List<Album> album) {
        albums = album;
    }

    @Override
    public String toString() {
        return "Artist [Albums=" + albums + "]";
    }

}
