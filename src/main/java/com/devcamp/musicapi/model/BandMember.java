package com.devcamp.musicapi.model;

public class BandMember extends Composer{

    private String instrument ;

    public BandMember(String firstname, String lastname, String stagename, String instrument) {
        super(firstname, lastname, stagename);
        this.instrument = instrument;
    }

    public String getInstrument() {
        return instrument;
    }

    public void setInstrument(String instrument) {
        this.instrument = instrument;
    }

    @Override
    public String toString() {
        return "BandMember [instrument=" + instrument + "]";
    }

}
