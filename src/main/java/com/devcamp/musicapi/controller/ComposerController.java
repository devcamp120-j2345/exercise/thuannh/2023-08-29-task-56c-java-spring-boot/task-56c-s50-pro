package com.devcamp.musicapi.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.musicapi.model.Artist;
import com.devcamp.musicapi.model.Band;
import com.devcamp.musicapi.model.Composer;
import com.devcamp.musicapi.service.ComposerService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class ComposerController {
    private ComposerService composerService;
    @GetMapping("/bands")
    public ArrayList<Band> getAllBand(){
        ArrayList<Band> allBand = composerService.getBands();
        return allBand ;
    }

    @GetMapping("/artists")
    public ArrayList<Artist> getAllArtist(){
        ArrayList<Artist> allArtists = composerService.getArtists();
        return allArtists ;
    }

    @GetMapping("/composeres")
    public ArrayList<Composer> getAllComposer(){
        ArrayList<Composer> allComposers = composerService.getComposeres();
        return allComposers ;
    }
}
