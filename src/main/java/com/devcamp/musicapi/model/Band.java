package com.devcamp.musicapi.model;

import java.util.List;

public class Band {
    
    private String bandname ;
    private List<BandMember> members ;
    private List<Album> albums ;
    
    public Band(String bandname, List<BandMember> members, List<Album> albums) {
        this.bandname = bandname;
        this.members = members;
        this.albums = albums;
    }

    public String getBandname() {
        return bandname;
    }

    public void setBandname(String bandname) {
        this.bandname = bandname;
    }

    public List<BandMember> getMembers() {
        return members;
    }

    public void setMembers(List<BandMember> members) {
        this.members = members;
    }

    public List<Album> getAlbums() {
        return albums;
    }

    public void setAlbums(List<Album> albums) {
        this.albums = albums;
    }

    @Override
    public String toString() {
        return "Band [albums=" + albums + ", bandname=" + bandname + ", members=" + members + "]";
    }
    

}
